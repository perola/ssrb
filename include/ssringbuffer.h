// -*- c++ -*-
// Originally from https://msmvps.com/blogs/vandooren/archive/2007/01/05/creating-a-thread-safe-producer-consumer-queue-in-c-without-using-locks.aspx
//Copyright (c) 2006 Bruno van Dooren
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef ssringbuffer_h
#define ssringbuffer_h


#define SSRB_USE_ATOMICS 1

#ifdef SSRB_USE_ATOMICS
#include <atomic>

/** Cross platform (c++11) implementation of van Doreens work.
 * @author Ola Nilsson per.ola.nilsson@gmail.com
 */
template <class T, int size> class ssringbuffer {
private:
  std::atomic<int> ReadIndex;
  std::atomic<int> WriteIndex;
  T *Data;

public:
  ssringbuffer() : ReadIndex(0), WriteIndex(0), Data(new T[(size + 1)]) {}
  ~ssringbuffer() { delete[] Data; }
  typedef T value_type;

  int capacity() const { return size; }
  bool push(const T &element) {
    int WriteIndexInt = WriteIndex.load();
    int nextWriteIndex = (WriteIndexInt + 1) % (size + 1);
    if (nextWriteIndex == ReadIndex.load()) // queue is full
      return false;

    Data[WriteIndexInt] = element;
    WriteIndex.store(nextWriteIndex,
                     std::memory_order_release); // no reorder from above,
    // the new write index store must happen after new data is written 
    return true;
  }

  bool pop(T &element) {
    int ReadIndexInt = ReadIndex.load();
    if (ReadIndexInt == WriteIndex.load())
      return false;

    // compute next index
    int nextReadIndex = (ReadIndexInt + 1) % (size + 1);
    element = Data[ReadIndexInt];

    ReadIndex.store(nextReadIndex,
                    std::memory_order_release); // no reorder from above,
    // the new read index store must happen after the return element is copied 

    return true;
  }
};

#else
// otherwise use locking
#include <mutex>

template <class T, int size> class ssringbuffer {
private:
  std::mutex SerializerMutex;

  int ReadIndex;
  int WriteIndex;
  T *Data;

public:
  ssringbuffer() : ReadIndex(0), WriteIndex(0), Data(new T[(size + 1)]) {}
  ~ssringbuffer() { delete[] Data; }
  typedef T value_type;

  int capacity() const { return size; }
  bool push(const T &element) {
    std::lock_guard<std::mutex> lock(SerializerMutex);
    int nextWriteIndex = (WriteIndex + 1) % (size + 1);
    if(nextWriteIndex == ReadIndex)
      return false;
    Data[WriteIndex] = element;
    WriteIndex = nextWriteIndex;
    return true;
  }

  bool pop(T &element) {
  std::lock_guard<std::mutex> lock(SerializerMutex);
  if(ReadIndex == WriteIndex)
    return false;

  int nextReadIndex = (ReadIndex + 1) % (size + 1);
  element = Data[ReadIndex];
  ReadIndex = nextReadIndex;
  return true;
  }
};
#endif // implementation switcher

#endif // ssringbuffer_h
