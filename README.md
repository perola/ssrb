# ssrb #

- a single producer single consumer lock free ringbuffer cross platform implementation in c++
- to build the test project Qt 5 and a c++11 supporting compiler is needed
- in order to use the tests as benchmark, a better strategy is probably to precompute a random data feed
- there's a locking version ifdefed away in the include for reference

This work is a re-implementation of https://msmvps.com/blogs/vandooren/archive/2007/01/05/creating-a-thread-safe-producer-consumer-queue-in-c-without-using-locks.aspx
