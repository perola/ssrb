TEMPLATE = app
TARGET = tst_ssringbuffer

SOURCES += tst_ssringbuffer.cpp

QT += testlib

INCLUDEPATH += ../include

mac {
  CONFIG -= app_bundle
}
