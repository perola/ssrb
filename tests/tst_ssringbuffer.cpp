#include <QtTest/QtTest>

#include <QThread>
#include <QDebug>

#include "ssringbuffer.h"
#include <algorithm>

#include <boost/random/mersenne_twister.hpp> // random was in
                                             // namespace boost before
                                             // (1.41), now in
                                             // boost::random (1.53)

class tst_ssringbuffer : public QObject {
  Q_OBJECT
public:
  tst_ssringbuffer() {}
  ~tst_ssringbuffer() {}
private slots:
  void verify_construction();
  void verify_fill_empty();
  void verify_message_stream();
};

/* Verify construction invariants
 */
void tst_ssringbuffer::verify_construction() {
  ssringbuffer<bool, 12> boolRing;
  QCOMPARE(boolRing.capacity(), 12);
  bool b;
  QVERIFY(!boolRing.pop(b));
}

template <class ring_t> typename ring_t::value_type spinning_pop(ring_t &ring) {
  typename ring_t::value_type ret;
  while (true)
    if (ring.pop(ret))
      break;
  return ret;
}

/* Verify api usage of the queue by filling it to the known max and
   then empty all.
 */
void tst_ssringbuffer::verify_fill_empty() {
  ssringbuffer<char, 10> cring;

  for (char i = 0; i < cring.capacity(); i++)
    QVERIFY(cring.push(i)); // should be able to push capacity items

  char msg;
  QVERIFY(!cring.push(msg)); // should be full

  for (char i = 0; i < cring.capacity(); i++) {
    QVERIFY(cring.pop(msg)); // should be able to pop capacity items
    QCOMPARE(msg, i);        // should follow the index
  }

  QVERIFY(!cring.pop(msg)); // should be empty
}

/* A non-trivial 'Message' type that contains random data.
   Used for verification of data transfer.
 */
struct Message {
  int m_Data[256];
  Message() { std::fill(m_Data, m_Data + 256, 0); }
  Message(boost::mt19937 &gen) { std::generate(m_Data, m_Data + 256, gen); }
  bool operator==(const Message &msg) const {
    return std::equal(m_Data, m_Data + 256, msg.m_Data);
  }
};

typedef ssringbuffer<Message, 100> buffer_t;

/* A producer
 */
class Producer : public QThread {
public:
  Producer(buffer_t &buffer, int num) : buffer(buffer), num(num) {}
  void run() {
    for (int i = 0; i < num; i++) {
      Message msg(gen);
      while (true) {
        if (buffer.push(msg)) {
          break;
        }
      }
    }
  }

private:
  boost::mt19937 gen;
  buffer_t &buffer;
  int num;
};

/* Create a message stream from random, but known data, using boost.
   Send data from a "producer" to a "consumer" and validate bytes that
   arrive.
 */
void tst_ssringbuffer::verify_message_stream() {
  buffer_t ring;
  Producer producer(ring, 1013037);

  // make sure thread is started before we continue
  QEventLoop loop;
  connect(&producer, SIGNAL(started()), &loop, SLOT(quit()));
  producer.start();
  loop.exec();

  QBENCHMARK{
    boost::mt19937 gen2;
    while (producer.isRunning()) {
      Message msg = spinning_pop(ring);
      Message mGood(gen2);
      QCOMPARE(msg, mGood);
    }
    
    Message msg;
    // no more production just consuming
    while (ring.pop(msg)) {
      Message mGood(gen2);
      QCOMPARE(msg, mGood);
    }
    QVERIFY(producer.wait());
  }
}

QTEST_MAIN(tst_ssringbuffer)
#include "tst_ssringbuffer.moc"
